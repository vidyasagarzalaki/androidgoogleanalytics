﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace Analytics
{
	[Activity ( Label = "Analytics" , MainLauncher = true , Icon = "@mipmap/icon" )]
	public class MainActivity : Activity
	{
		int count = 1;
		protected override void OnCreate ( Bundle savedInstanceState )
		{
			base.OnCreate ( savedInstanceState );

			// Set our view from the "main" layout resource
			SetContentView ( Resource.Layout.Main );
			//Initialize GAService
			GAService.GetGASInstance().Initialize(this);
			//Trackpage
			GAService.GetGASInstance().Track_App_Page("MainScreen");
			Button button = FindViewById<Button> ( Resource.Id.myButton );
			
			button.Click += delegate
			{
				// Track an event
				GAService.GetGASInstance().Track_App_Event(GAEventCategory.DisCoverButton, "DisCoverButton Clicked");
				button.Text = string.Format ( "{0} clicks!" , count++ );
			};
		}
	}
}
//Component->Google Play Service-Analytics.
//link(example)->https://theconfuzedsourcecode.wordpress.com/2015/07/06/lets-add-google-analytics-to-your-xamarin-android-app-_/

